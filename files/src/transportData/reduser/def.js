const DEFAULT_METHOD = "DEFAULT_METHOD";

export default function (state = [], action){
	switch (action.type) {
		case DEFAULT_METHOD:{
			let newObj = [].concat( action.payload );
			return newObj;
		}

		default:
			return state
	}
}
