import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactFullpage from '@fullpage/react-fullpage';
import { dpath } from '../Main'
import axios from 'axios';
import FirstScreen from './FirstScreen';
import SecondScreen from './SecondScreen';
import Footer from '../Footer';

const pluginWrapper = () => {
    require('fullpage.js/vendors/scrolloverflow');
};

class Functionality extends Component{
    constructor(props){
        super(props);
        this.state = {
            sectionsShowed:[0],
            activedSection:0,
        };
        this.showSection = this.showSection.bind(this);
        this.setActivedSection = this.setActivedSection.bind(this);
    }

    showSection(index){
        let showed = this.state.sectionsShowed;
        showed[index] = true;
        this.setState({
            sectionsShowed: showed,
        })
    }
    setActivedSection(id){
        this.setState({
            activedSection: id,
        })
    }
    componentDidMount() {
      let adr = this.props.ownProps.location.pathname;
      adr = adr.replace('/', '');

      axios({baseURL: dpath+'/'+adr+'/?ax=1', mode: "no-cors", crossDomain: true})
           .then(
               (res) => {


                 con = JSON.stringify(res.data);
                 let  con = JSON.parse(con);

                 this.setState({
                   isLoaded: true,
                   content: con
                 });


            }
           )
    }

    render(){
        let { showHeader, hideHeader } = this.props;
        let { showSection, setActivedSection } = this;
        const fullpageOptions = {
            lockAnchors: true,
            callbacks: ["onLeave", "afterLoad"],
            scrollOverflow: true,
            dragAndMove:true,
            pluginWrapper:pluginWrapper,
            onLeave: (origin, destination, direction) => {
                showSection(origin.index);
                showSection(destination.index);
                setActivedSection(destination.index);
                if(origin.index === 0){
                    hideHeader();
                }
                if(origin.index === 1 && direction === "up"){
                    showHeader();
                }

            }
        };

        let cont = this.state.content;
        let title = '';
        let content = '';
        let paslaugosb_1 = '';
        let paslaugosb_2 = '';
        let paslaugosb_3 = '';
        let paslaugost_1 = '';
        let paslaugost_2 = '';
        let paslaugost_3 = '';


        if (typeof cont != 'undefined') {
            title = cont.title;
            content = cont.content;
            paslaugosb_1 = cont.paslaugosb_1;
            paslaugosb_1 = paslaugosb_1.split(',');
            paslaugosb_2 = cont.paslaugosb_2;
            paslaugosb_2 = paslaugosb_2.split(',');
            paslaugosb_3 = cont.paslaugosb_3;
            paslaugosb_3 = paslaugosb_3.split(',');
            paslaugost_1 = cont.paslaugost_1;
            paslaugost_2 = cont.paslaugost_2;
            paslaugost_3 = cont.paslaugost_3;
        }

        return (
            <div className="full-page services">
                <ReactFullpage {...fullpageOptions}
                    render={({state, fullpageApi}) => {
                        return (
                            <div id="fullpage-wrapper">
                                <div className={"section services-first fp-noscroll"} data-is-showed={this.state.sectionsShowed[0]?'show':''}>
                                    <FirstScreen title={title} content={content} fullpageApi={fullpageApi}/>
                                </div>
                                <div className={"section services-second fp-noscroll"} data-is-showed={this.state.sectionsShowed[1]?'show':''}>
                                    <SecondScreen paslaugosb_1={paslaugosb_1} paslaugosb_2={paslaugosb_2} paslaugosb_3={paslaugosb_3} paslaugost_1={paslaugost_1} paslaugost_2={paslaugost_2} paslaugost_3={paslaugost_3} fullpageApi={fullpageApi}/>
                                </div>
                                <div className={"section services-third fp-noscroll"} data-is-showed={this.state.sectionsShowed[2]?'show':''}>
                                    <Footer fullpageApi={fullpageApi} lang={this.props.lang}/>
                                </div>
                            </div>
                        )
                    }
                    }
                />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(Functionality));
