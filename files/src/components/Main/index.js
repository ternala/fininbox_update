import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Router from '../Router/';
import axios from 'axios';

export let dpath = '';
export let felems = {};



class app extends Component{
    constructor(props){
        super(props);
        this.state ={
            header:true,
            menu:false
        };

        this.vrb = 'bnd2';
        this.hideHeader = this.hideHeader.bind(this);
        this.showHeader = this.showHeader.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);

    }

    componentDidMount() {
              axios({baseURL: dpath+'/?ax=2', mode: "no-cors", crossDomain: true})
                   .then(
                       (res) => {

                         con = JSON.stringify(res.data);
                         let con = JSON.parse(con);

                         this.setState({
                           isLoaded: true,
                           menuc: con.menu,
                           lang: con.lang
                         });


                    }
                   )

    }

    hideHeader(){
        console.log('hide');
        this.setState({
            header:false
        })
    }

    showHeader(){
        console.log('show');
        this.setState({
            header:true
        })
    }

    toggleMenu(){
        this.setState({menu:!this.state.menu})
    }
    render(){
        let activedElement = '';

        let con = this.state.menuc;
        let check = this.props.ownProps.location.pathname;
        let menu = '';
        let last = '';


      if (typeof con != 'undefined') {


           Object.keys(con).forEach(function(key, index, array) {


            if (index === array.length - 1){
             if(check === '/'+key || check === '/'+key+'/'){
              last = '<a href="/'+key+'" class="btn active">'+con[key]+'<div class="arrow"><hr/><i></i></div></a>';
             }else{
               last = '<a href="/'+key+'" class="btn">'+con[key]+'<div class="arrow"><hr/><i></i></div></a>';
             }
            }else{
                if(check === '/'+key || check === '/'+key+'/'){
                    activedElement = key;
                    menu += '<li class="active"><a href="/'+key+'">'+con[key]+'</a></li>';
                }else{
                    menu += '<li><a href="/'+key+'">'+con[key]+'</a></li>';
                }
            }

           })

      }


        return (

            <Fragment>
                <img src={require('../../view/img/0-logo-white.svg')} alt="" className="logo-fixed"/>
                <header className={this.state.header?'':'hide'}>
                    <div className="container">
                        <div className="d-flex f-between f-align-center">
                            <div className="left">
                                <Link to="" className="logo">
                                    <img src={require('../../view/img/logo.svg')}/>
                                </Link>
                            </div>
                            <div className={"wrapper-menu" + (this.state.menu?' show': '')}>
                                <div className="d-flex f-between f-align-center another-path">

                                    <div className="center">



                                        <ul className="d-flex f-align-center f-center" dangerouslySetInnerHTML={{__html: menu}}>
                                        </ul>

                                    </div>
                                    <div className="right" dangerouslySetInnerHTML={{__html: last}}>


                                    </div>
                                </div>
                                <div className="sandwich-icon" onClick={this.toggleMenu}>
                                    <i></i><i></i><i></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <Router showHeader={this.showHeader} hideHeader={this.hideHeader} lang={this.state.lang}/>

            </Fragment>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(app));
