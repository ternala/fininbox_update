import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactFullpage from '@fullpage/react-fullpage';

import axios from 'axios';
import { dpath } from '../Main'

import FistScreen from './FirstScreen/';
import SecondScreen from './SecondScreen/';
import ThirdScreen from './ThirdScreen/';
import FourScreen from './FourScreen/';
import FiveScreen from './FiveScreen/';
import SixScreen from './SixScreen/';
import SevenScreen from './SevenScreen/';
import EightScreen from './EightScreen';
import NineScreen from './NineScreen';
import Footer from './TenScreen';


const pluginWrapper = () => {
    require('fullpage.js/vendors/scrolloverflow');
};

class home extends Component{
    constructor(props){
        super(props);
        this.state = {
            sectionsShowed:[],
            activedSection:0,
        };
        this.showSection = this.showSection.bind(this);
        this.setActivedSection = this.setActivedSection.bind(this);
    }

    componentDidMount() {
      let adr = this.props.ownProps.location.pathname;
      adr = adr.replace('/', '');

      axios({baseURL: dpath+'/?ax=1', mode: "no-cors", crossDomain: true})
           .then(
               (res) => {


                 con = JSON.stringify(res.data);
                 let  con = JSON.parse(con);

                 this.setState({
                   isLoaded: true,
                   content: con
                 });


            }
           )
    }

    showSection(index){
        let showed = this.state.sectionsShowed;
        showed[index] = true;
        this.setState({
            sectionsShowed: showed,
        })
    }
    setActivedSection(id){
        this.setState({
            activedSection: id,
        })
    }

	render(){
	    let { showHeader, hideHeader } = this.props;
	    let { showSection, setActivedSection } = this;
        const fullpageOptions = {
            lockAnchors: true,
            callbacks: ["onLeave", "afterLoad"],
            scrollOverflow: true,
            dragAndMove:true,
            pluginWrapper:pluginWrapper,
            onLeave: (origin, destination, direction) => {
                showSection(origin.index);
                showSection(destination.index);
                setActivedSection(destination.index);
                if(origin.index === 0){
                    hideHeader();
                }
                if(origin.index === 1 && direction === "up"){
                    showHeader();
                }

            }
        };


        let cont = this.state.content;
        let title = '';
        let content_a = [];
        let content_t = [];
        let content_n = [];
        let content_b = [];


        if (typeof cont != 'undefined') {
            title = cont.title;
            content_a[2] = cont.content2_a;
            content_t[2] = cont.content2_t;
            content_n[2] = cont.content2_n;

            content_a[3] = cont.content3_a;
            content_t[3] = cont.content3_t;
            content_n[3] = cont.content3_n;

            content_a[4] = cont.content4_a;
            content_t[4] = cont.content4_t;
            content_n[4] = cont.content4_n;

            content_a[5] = cont.content5_a;
            content_t[5] = cont.content5_t;
            content_n[5] = cont.content5_n;

            content_a[6] = cont.content6_a;
            content_t[6] = cont.content6_t;
            content_b[6] = cont.content6_b;

            content_a[71] = cont.content7_a1;
            content_t[71] = cont.content7_t1;
            content_a[72] = cont.content7_a2;
            content_t[72] = cont.content7_t2;

            content_b[9] = cont.content9_b;

            content_a[101] = cont.content10_a1;
            content_t[101] = cont.content10_t1;
            content_a[102] = cont.content10_a2;
            content_n[102] = cont.content10_n2;
        }

		return (
		    <div className="full-page">
                <div className="container-for-cube">
                    <div className="wrapper-cube" data-active-section={this.state.activedSection}>
                        <div className="cube">
                            <div className="back side"></div>
                            <div className="left side"></div>
                            <div className="right side"></div>
                            <div className="top side"></div>
                            <div className="bottom side"></div>
                            <div className="front side"></div>
                        </div>
                    </div>
                </div>
                <ReactFullpage {...fullpageOptions}
                    render={({state, fullpageApi}) => {
                        return (
                            <div id="fullpage-wrapper">
                                <div className={"section first-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[0]?'show':''}>
                                    <FistScreen fullpageApi={fullpageApi} hideHeader={this.props.hideHeader} showHeader={this.props.showHeader}/>
                                </div>
                                <div className={"section second-section fp-noscroll"} id="second-section" data-is-showed={this.state.sectionsShowed[1]?'show':''}>
                                    <SecondScreen title={content_a[2]} content={content_t[2]} link={content_n[2]} lang={this.props.lang}/>
                                </div>
                                <div className={"section third-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[2]?'show':''}>
                                    <ThirdScreen title={content_a[3]} content={content_t[3]} link={content_n[3]} lang={this.props.lang} />
                                </div>
                                <div className={"section four-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[3]?'show':''}>
                                    <FourScreen title={content_a[4]} content={content_t[4]} link={content_n[4]} lang={this.props.lang} />
                                </div>
                                <div className={"section five-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[4]?'show':''}>
                                    <FiveScreen title={content_a[5]} content={content_t[5]} link={content_n[5]} lang={this.props.lang} />
                                </div>
                                <div className={"section six-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[5]?'show':''}>
                                    <SixScreen title={content_a[6]} content={content_t[6]} blocks={content_b[6]} lang={this.props.lang}/>
                                </div>
                                <div className={"section seven-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[6]?'show':''}>
                                    <SevenScreen title1={content_a[71]} content1={content_t[71]} title2={content_a[72]} content2={content_t[72]} lang={this.props.lang} />
                                </div>
                                <div className={"section eight-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[7]?'show':''}>
                                    <EightScreen lang={this.props.lang}/>
                                </div>
                                <div className={"section nine-section fp-noscroll"} data-is-showed={this.state.sectionsShowed[8]?'show':''}>
                                    <NineScreen blocks={content_b[9]} />
                                </div>
                                <div className={"section ten-section"} id="end-section" data-is-showed={this.state.sectionsShowed[9]?'show':''}>
                                    <Footer title1={content_a[101]} content1={content_t[101]} title2={content_a[102]} link2={content_n[102]} lang={this.props.lang} lang={this.props.lang}/>
                                </div>
                            </div>
                        )
                    }
                }/>
            </div>
		);
	}
};

export default withRouter(connect(
	(state, ownProps) => ({
		ownProps
	})
)(home));
