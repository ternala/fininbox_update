import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class SixScreen extends Component{
    render(){
      let lang = '';
      let title1 = [];
      let title2 = [];

      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
        title1 = '';
        title1 = this.props.title1;

        title1 = title1.split(",");
        title2 = '';
        title2 = this.props.title2;
        title2 = title2.split(",");
      }

        return (
            <div className="container">
                <img src={require('../../../view/img/5-background.svg')} alt="" className="yellow-fragment fragment-bg"/>
                <h2>
                    {lang.ka_siulome}
                </h2>
                <div className="blocks d-flex">
                    <div className="left">
                        <div className="top">
                            <h3>{title1[0]}</h3>
                            <h4>{title1[1]}</h4>
                            <img src={require('../../../view/img/purple-arrow-right.svg')} alt="" className='arrow-right'/>
                            <img src={require('../../../view/img/left-icon.svg')} alt="" className="icon"/>
                        </div>
                        <img src={require('../../../view/img/6a-background.svg')} alt="" className="bg"/>
                        <div className="content" dangerouslySetInnerHTML={{__html: this.props.content1}}>

                        </div>
                    </div>
                    <div className="right">
                        <div className="top">
                            <h3>{title2[0]}</h3>
                            <h4>{title2[1]}</h4>
                            <img src={require('../../../view/img/green-arrow-right.svg')} alt="" className='arrow-right'/>
                            <img src={require('../../../view/img/right-icon.svg')} alt="" className="icon"/>
                        </div>
                        <img src={require('../../../view/img/6b-background.svg')} alt="" className="bg"/>
                        <div className="content" dangerouslySetInnerHTML={{__html: this.props.content2}}>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(SixScreen));
