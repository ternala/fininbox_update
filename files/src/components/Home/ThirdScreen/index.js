import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class ThirdScreen extends Component{
    render(){
      let lang = '';

      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
      }
        return (
            <div className="container">
                <img src={require('../../../view/img/2-background.svg')} alt="" className="yellow-fragment fragment-bg"/>
                <h2 dangerouslySetInnerHTML={{__html: this.props.title}}>

                </h2>
                <div className="d-flex main-row">
                    <div className="block-7 left with-content">
                        <p dangerouslySetInnerHTML={{__html: this.props.content}}>
                        </p>

                        <a href={this.props.link} className="btn">
                            {lang.placiau}
                            <div className="arrow">
                                <hr/><i></i>
                            </div>
                        </a>
                    </div>
                    <div className="block-5">
                    </div>
                </div>
                <img className="background-symbol second" src={require('../../../view/img/background_symbol2.svg')} />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(ThirdScreen));
