import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

class SecondScreen extends Component{
  constructor(props){

      super(props);
      this.state = {
          loaded:false,
          items: []
      };
  }
  componentDidMount() {

   }



    render(){
      let lang = '';

      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
      }
        return (
            <div className="container">
                <img src={require('../../../view/img/1-background.svg')} alt="" className="purple-fragment fragment-bg"/>
                <h2 dangerouslySetInnerHTML={{__html: this.props.title}}>

                </h2>
                <div className="d-flex main-row">
                    <div className="block-5">

                    </div>
                    <div className="block-7 right with-content">
                        <p dangerouslySetInnerHTML={{__html: this.props.content}}>
                        </p>

                        <a href={this.props.link} className="btn">
                            {lang.placiau}
                            <div className="arrow">
                                <hr/><i></i>
                            </div>
                        </a>
                    </div>
                </div>
                <img className="background-symbol first" src={require('../../../view/img/background_symbol1.svg')} />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(SecondScreen));
