import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class ThirdScreen extends Component{
    render(){
      let lang = '';

      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
      }
        return (
            <div className="container">
                <img src={require('../../../view/img/3-background.svg')} alt="" className="yellow-fragment fragment-bg"/>
                <div className="d-flex main-row">
                    <div className="block-5">

                    </div>
                    <div className="block-7 right  with-content">
                        <h2 dangerouslySetInnerHTML={{__html: this.props.title}}>

                        </h2>
                        <div className="text">
                            <p dangerouslySetInnerHTML={{__html: this.props.content}}>
                            </p>
                            <a href={this.props.link} className="btn">
                                {lang.placiau}
                                <div className="arrow">
                                    <hr/><i></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <img className="background-symbol second" src={require('../../../view/img/background_symbol3.svg')} />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(ThirdScreen));
