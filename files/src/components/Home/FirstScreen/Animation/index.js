import React from 'react'
import Lottie from 'react-lottie';
import logoStart from './logo-small.json';
import logoEnd from './logo-end.json';


export default class LottieControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            direction: 1,
            compleate: false,
            count: 0,
            sendCallback:false
        };
    }

    nextTick() {
        if(!this.state.compleate){
            if (this.state.direction === -1) {
                if (this.props.statusVideo && this.state.count > 1) {
                    this.setState({
                        compleate: true
                    })
                } else {
                    let count = this.state.count;
                    this.setState({
                        direction: 1,
                        count: (count + 1)
                    })
                }
            } else {
                this.setState({
                    direction: -1
                })
            }
        }
    }

    compleateAnimation() {
        if(!this.state.sendCallback){
            this.props.onComplete('video');
            this.setState({
                sendCallback:true
            })
        }
    }
    renderAnimation(){
        if(this.state.compleate){
            const defaultOptions = {
                autoplay: true,
                loop: false,
                animationData: logoEnd,
                rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice'
                }
            };

            return (<Lottie
                        key={"end"}
                        options={defaultOptions}
                        direction={this.state.direction}
                        eventListeners={[
                            {
                                eventName: 'complete',
                                callback: () => this.compleateAnimation(),
                            },
                        ]}
                    />)
        }else{
        //    console.log('start');
            const defaultOptions = {
                autoplay: true,
                loop: false,
                animationData: logoStart,
                rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice'
                }
            };

            return (<Lottie
                        key={"start"}
                        options={defaultOptions}
                        direction={this.state.direction}
                        eventListeners={[
                            {
                                eventName: 'complete',
                                callback: () => this.nextTick(),
                            },
                        ]}
                    />)
        }
    }

    render() {
      //  console.log(this.state.direction);
        return (
            <div className={"animation-home"}>
                {this.renderAnimation()}
            </div>
        )
    }
}
