import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Animation from './Animation/';
import Video from './Video/';

class home extends Component{
    constructor(props){
        super(props);
        this.state = {
            show:'animation',
            videoReady:false,
            videoPlay: false
        };
        this.changeShow =  this.changeShow.bind(this);
        this.setVideoReady = this.setVideoReady.bind(this);
        this.props.hideHeader();
    }

    changeShow(content){
        this.setState({
            show:content,
            videoPlay:true
        })
        this.props.showHeader();
    }

    setVideoReady(){
        this.setState({
            videoReady:true
        })
    }


    render(){
      //  console.log( this.state.show);
        return (
            <div>
                <div className={"wrapper-animation" + (this.state.show === "animation"?" show":"")}>
                    <Animation onComplete={this.changeShow} statusVideo={this.state.videoReady}/>
                </div>
                <Video fullpageApi={this.props.fullpageApi} setVideoReady={this.setVideoReady} videoPlay={this.state.videoPlay}/>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(home));
