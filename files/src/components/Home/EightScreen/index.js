import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class EightScreen extends Component{
    render(){
      let lang = '';


      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
      }
        return (
            <div className="container">
                <div className="blocks">
                    <h2>
                        {lang.kiek_kainuoja}
                    </h2>
                    <div className="column">
                        <div className="block-left">
                            <div className="content">
                                <div className="inner">
                                    <div className="wrap-text">
                                        <img src={require('../../../view/img/7-setup_icon.svg')} alt=""/>
                                        <h3>{lang.setup_fee}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="block-right">
                            <div className="content">
                                <div className="inner">

                                    <div className="wrap-text">
                                        <img src={require('../../../view/img/7-monthly_icon.svg')} alt=""/>
                                        <h3>{lang.maintenance_fee}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="plus">
                        <img src={require('../../../view/img/icon-plus.svg')} alt=""/>
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(EightScreen));
