import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactFullpage from '@fullpage/react-fullpage';
import axios from 'axios';
import { dpath } from '../Main'

import FirstScreen from './FirstScreen';
import SecondScreen from './SecondScreen';
import ThirdScreen from './ThirdScreen';
import Footer from '../Footer';

const pluginWrapper = () => {
    require('fullpage.js/vendors/scrolloverflow');
};





class Functionality extends Component{
    constructor(props){
        super(props);
        this.state = {
            sectionsShowed:[0],
            activedSection:0,
            loaded:false
        };

        this.vrb = 'check';

        this.showSection = this.showSection.bind(this);
        this.setActivedSection = this.setActivedSection.bind(this);
        this.setLoaded = this.setLoaded.bind(this);

    }
    componentDidMount() {
      let adr = this.props.ownProps.location.pathname;
      adr = adr.replace('/', '');

      axios({baseURL: dpath+'/'+adr+'/?ax=1', mode: "no-cors", crossDomain: true})
           .then(
               (res) => {


                 con = JSON.stringify(res.data);
                 let  con = JSON.parse(con);

                 this.setState({
                   isLoaded: true,
                   content: con
                 });


            }
           )
    }


    showSection(index){
        let showed = this.state.sectionsShowed;
        showed[index] = true;
        this.setState({
            sectionsShowed: showed,
        })
    }
    setActivedSection(id){
        this.setState({
            activedSection: id,
        })
    }
    setLoaded(){
        this.setState({
            loaded:true
        })
    }

    render(){

        let { showHeader, hideHeader } = this.props;
        let { showSection, setActivedSection } = this;
        const fullpageOptions = {
            lockAnchors: true,

            callbacks: ["onLeave", "afterLoad"],
            scrollOverflow: true,
            dragAndMove:true,
            pluginWrapper:pluginWrapper,
            onLeave: (origin, destination, direction) => {
                showSection(origin.index);
                showSection(destination.index);
                setActivedSection(destination.index);
                if(origin.index === 0){
                    hideHeader();
                }
                if(origin.index === 1 && direction === "up"){
                    showHeader();
                }

            }
        };

        let cont = this.state.content;
        let title = '';
        let content = '';
        let antras_1 = '';
        let antras_2 = '';
        let trecias_1 = '';
        let trecias_2 = '';

        if (typeof cont != 'undefined') {
            title = cont.title;
            content = cont.content;
            antras_1 = cont.antras_1;
            antras_2 = cont.antras_2;
            trecias_1 = cont.trecias_1;
            trecias_2 = cont.trecias_2;
        }

        return (
            <div className="full-page about">
                <ReactFullpage {...fullpageOptions}
                    render={({state, fullpageApi}) => {
                        return (
                            <div id="fullpage-wrapper">
                                <div className={"section about-first fp-noscroll"} data-is-showed={this.state.sectionsShowed[0]?'show':''}>
                                    <FirstScreen fullpageApi={fullpageApi} title={title} content={content} setLoaded={this.setLoaded}/>
                                </div>
                                <div className={"section about-second fp-noscroll"} data-is-loaded={(this.state.loaded? "loaded": '')} data-is-showed={this.state.sectionsShowed[1]?'show':''}>
                                    <SecondScreen fullpageApi={fullpageApi} content_1={antras_1}  content_2={antras_2}/>
                                </div>
                                <div className={"section about-third fp-noscroll"} data-is-showed={this.state.sectionsShowed[2]?'show':''}>
                                    <ThirdScreen fullpageApi={fullpageApi} content_1={trecias_1}  content_2={trecias_2}/>
                                </div>
                                <div className={"section about-four fp-noscroll"} data-is-showed={this.state.sectionsShowed[3]?'show':''}>

                                    <Footer fullpageApi={fullpageApi} lang={this.props.lang}/>
                                </div>
                            </div>
                        )
                    }
                    }
                />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(Functionality));
