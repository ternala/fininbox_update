import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import bgFragment2 from '../../../view/img/triangle-green-left.svg';

class ServicesFirst extends Component{
    render(){
        return (
            <div className={"container"}>
                <img className="fragment-bg" src={bgFragment2} alt=""/>
                <img className="background-symbol first" src={require('../../../view/img/background_symbol4.svg')} />
                <div className="text d-flex f-column f-between">
                    <p dangerouslySetInnerHTML={{__html: this.props.content_1}}></p>
                    <div className="left">
                        <h4 dangerouslySetInnerHTML={{__html: this.props.content_2}}></h4>
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(ServicesFirst));
