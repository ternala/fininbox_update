import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactFullpage from '@fullpage/react-fullpage';
import axios from 'axios';
import { dpath } from '../Main'

import FistScreen from './FirstScreen/';
import SecondScreen from './SecondScreen/';
import Footer from '../Footer';

const pluginWrapper = () => {
    require('fullpage.js/vendors/scrolloverflow');
};

class home extends Component{
    constructor(props){
        super(props);
        this.state = {
            sectionsShowed:[],
            activedSection:0,
        };
        this.showSection = this.showSection.bind(this);
        this.setActivedSection = this.setActivedSection.bind(this);
    }

    componentDidMount() {
      let adr = this.props.ownProps.location.pathname;
      adr = adr.replace('/', '');

      axios({baseURL: dpath+'/'+adr+'/?ax=1', mode: "no-cors", crossDomain: true})
           .then(
               (res) => {


                 con = JSON.stringify(res.data);
                 let  con = JSON.parse(con);

                 this.setState({
                   isLoaded: true,
                   content: con
                 });


            }
           )
    }

    showSection(index){
        let showed = this.state.sectionsShowed;
        showed[index] = true;
        this.setState({
            sectionsShowed: showed,
        })
    }
    setActivedSection(id){
        this.setState({
            activedSection: id,
        })
    }

    render(){
        let { showHeader, hideHeader, fullpageApi } = this.props;
        let { showSection, setActivedSection } = this;
        const fullpageOptions = {
            lockAnchors: true,
            callbacks: ["onLeave", "afterLoad"],
            scrollOverflow: true,
            dragAndMove:true,
            pluginWrapper:pluginWrapper,
            onLeave: (origin, destination, direction) => {
                showSection(origin.index);
                showSection(destination.index);
                setActivedSection(destination.index);
                if(origin.index === 0){
                    hideHeader();
                }
                if(origin.index === 1 && direction === "up"){
                    showHeader();
                }
            }
        };

        let cont = this.state.content;
        let title = '';
        let content = '';
        let content0 = '';
        let content2 = '';

        if (typeof cont != 'undefined') {
            title = cont.title;
            content = cont.content;
            content0 = cont.content0;
            content2 = cont.content2;
        }

        return (
            <div className="full-page contacts">
                <ReactFullpage {...fullpageOptions}
                    render={({state, fullpageApi}) => {
                       return (
                           <div id="fullpage-wrapper">
                               <div className={"section contacts-first fp-noscroll"} data-is-showed={this.state.sectionsShowed[0]?'show':''}>
                                   <FistScreen  title={title} content={content} content0={content0} content2={content2} />
                               </div>
                               <div className={"section contacts-second fp-noscroll"} data-is-showed={this.state.sectionsShowed[1]?'show':''}>
                                   <SecondScreen state={state} fullpageApi={fullpageApi} lang={this.props.lang}/>
                               </div>
                               <div className={"section contacts-third fp-noscroll"} data-is-showed={this.state.sectionsShowed[2]?'show':''}>
                                   <Footer lang={this.props.lang}/>
                               </div>
                           </div>
                       )
                    }}/>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(home));
