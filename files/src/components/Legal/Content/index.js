import React, {Component} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import TrackVisibility from 'react-on-screen';

import leftTriangle from '../../../view/img/triangle-grey-left.svg';
import rightTriangle from '../../../view/img/triangle-grey-right.svg';

class TermsConditions extends Component{
    constructor(props){
        super(props);
        this.state = {
            loaded:false
        }
    }
    componentDidMount(){
        this.setState({
            loaded:true
        })
    }

    render(){
        return (
            <section className={"section text-page"  + (this.state.loaded?' loaded': '')}>
                <div className="container">
                    <div className="bgs">
                        <TrackVisibility once offset={150}>
                            {({ isVisible })=> (<img src={rightTriangle} className={"bg right-triangle"} alt="" data-visible={isVisible?'visible':'not-visible'}/>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={150}>
                            {({ isVisible })=> (<img src={leftTriangle} className={"bg left-triangle"} alt="" data-visible={isVisible?'visible':'not-visible'}/>)}
                        </TrackVisibility>
                    </div>
                    <TrackVisibility once offset={50}>
                        {({ isVisible })=> (<h1 data-visible={isVisible?'visible':'not-visible'}>Legal</h1>)}
                    </TrackVisibility>
                    <div className="text-block">
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<h3 data-visible={isVisible?'visible':'not-visible'}>Heading text 1</h3>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores cumque dolore dolorem doloribus eaque exercitationem facilis ipsum iste, maxime molestias obcaecati omnis provident reprehenderit temporibus vitae voluptate! Eum, omnis!Accusantium assumenda at, consequatur consequuntur delectus dolore doloremque ducimus eaque molestiae nesciunt nostrum repudiandae sit velit? Dolores ea earum, et exercitationem illo officiis perferendis similique sint velit? Blanditiis, provident, quia?Asperiores dolore, ducimus nemo perspiciatis possimus quam sint sit tempore voluptates. Architecto at atque distinctio dolore doloremque, ducimus expedita laboriosam minus officiis, quas qui quidem ratione saepe sit ut voluptatum?</p>)}
                    </TrackVisibility>
                    </div>
                    <div className="text-block">
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<h3 data-visible={isVisible?'visible':'not-visible'}>Heading text 1</h3>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<h4 data-visible={isVisible?'visible':'not-visible'}>Sub heading text example 1</h4>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores cumque dolore dolorem doloribus eaque exercitationem facilis ipsum iste, maxime molestias obcaecati omnis provident reprehenderit temporibus vitae voluptate! Eum, omnis!Accusantium assumenda at, consequatur consequuntur delectus dolore doloremque ducimus eaque molestiae nesciunt nostrum repudiandae sit velit? Dolores ea earum, et exercitationem illo officiis perferendis similique sint velit? Blanditiis, provident, quia?Asperiores dolore, ducimus nemo perspiciatis possimus quam sint sit tempore voluptates. Architecto at atque distinctio dolore doloremque, ducimus expedita laboriosam minus officiis, quas qui quidem ratione saepe sit ut voluptatum?</p>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores cumque dolore dolorem doloribus eaque exercitationem facilis ipsum iste, maxime molestias obcaecati omnis provident reprehenderit temporibus vitae voluptate! Eum, omnis!Accusantium assumenda at, consequatur consequuntur delectus dolore doloremque ducimus eaque molestiae nesciunt nostrum repudiandae sit velit? Dolores ea earum, et exercitationem illo officiis perferendis similique sint velit? Blanditiis, provident, quia?Asperiores dolore, ducimus nemo perspiciatis possimus quam sint sit tempore voluptates. Architecto at atque distinctio dolore doloremque, ducimus expedita laboriosam minus officiis, quas qui quidem ratione saepe sit ut voluptatum?</p>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores cumque dolore dolorem doloribus eaque exercitationem facilis ipsum iste, maxime molestias obcaecati omnis provident reprehenderit temporibus vitae voluptate! Eum, omnis!Accusantium assumenda at, consequatur consequuntur delectus dolore doloremque ducimus eaque molestiae nesciunt nostrum repudiandae sit velit? Dolores ea earum, et exercitationem illo officiis perferendis similique sint velit? Blanditiis, provident, quia?Asperiores dolore, ducimus nemo perspiciatis possimus quam sint sit tempore voluptates. Architecto at atque distinctio dolore doloremque, ducimus expedita laboriosam minus officiis, quas qui quidem ratione saepe sit ut voluptatum?</p>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores cumque dolore dolorem doloribus eaque exercitationem facilis ipsum iste, maxime molestias obcaecati omnis provident reprehenderit temporibus vitae voluptate! Eum, omnis!Accusantium assumenda at, consequatur consequuntur delectus dolore doloremque ducimus eaque molestiae nesciunt nostrum repudiandae sit velit? Dolores ea earum, et exercitationem illo officiis perferendis similique sint velit? Blanditiis, provident, quia?Asperiores dolore, ducimus nemo perspiciatis possimus quam sint sit tempore voluptates. Architecto at atque distinctio dolore doloremque, ducimus expedita laboriosam minus officiis, quas qui quidem ratione saepe sit ut voluptatum?</p>)}
                        </TrackVisibility>
                        <TrackVisibility once offset={100}>
                            {({ isVisible })=> (<p data-visible={isVisible?'visible':'not-visible'}><Link to="link">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</Link></p>)}
                        </TrackVisibility>
                    </div>
                </div>
            </section>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(TermsConditions));
