import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import def from './def.js';


const  allReducers = combineReducers ({
	def: def,
	routing: routerReducer,
});

export default allReducers
