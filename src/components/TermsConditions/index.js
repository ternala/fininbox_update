import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Content from './Content';
import Footer from '../Footer';
import TrackVisibility from "react-on-screen";

class TermsConditions extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <Fragment>
                <Content />
                <TrackVisibility once offset={100}>
                    {({ isVisible })=> (
                     <Footer visible={isVisible?'visible':'not-visible'}/>
                    )}
                </TrackVisibility>
            </Fragment>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(TermsConditions));