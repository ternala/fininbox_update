import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { dpath } from '../../Main'


import triangleBg from '../../../view/img/3-background-triangle.svg';


class FunctionalitySecond extends Component{
    constructor(props){
        super(props);
        this.state = {
            show:'animation',
            activeElement: false
        };
        this.changeShow =  this.changeShow.bind(this);
    }

    changeShow(content){
        this.setState({
            show:content
        })
    }
    setActive(el, e){
        console.log(e.target.tagName);
        if(!(e.target.classList.value.indexOf('close') !== -1 || e.target.tagName.toUpperCase() === 'I')) {
            this.setState({
                activeElement: el
            })
        }
    }
    removeActive(){
        this.setState({
            activeElement:false
        })
    }

    render(){
      let con = this.props.content;
      let pict = '';
      let blocksArr = [];
      if (typeof con != 'undefined') {
        Object.keys( con ).map(key=>{

          blocksArr.push(
            <div className={"block block-3" + (this.state.activeElement === key ? ' active': '')} key={ key } onClick={(e) => this.setActive( key, e )}>
              <div className="wrap">
                  <div className="inner">
                      <img src={ dpath + con[key].picture } alt=""/>
                      <h4>{ con[key].name }</h4>
                      <div className="arrow">
                          <hr/><i></i>
                      </div>

                      <div className="popup">
                          <div className="close" onClick={() => this.removeActive()}>
                              <i></i><i></i>
                          </div>
                          <h4>{ con[key].name }</h4>
                          <p>{ con[key].text }</p>
                      </div>
                  </div>
              </div>
          </div>
          )
        })
      }

        return (
            <div className="container">
                <img src={triangleBg} className="fragment-bg" alt=""/>
                <div className="blocks d-flex f-wrap f-center f-align-center">
                    {blocksArr}
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(FunctionalitySecond));
