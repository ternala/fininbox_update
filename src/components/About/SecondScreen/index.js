import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import bgFragment from '../../../view/img/bg-2.svg';
import bgFragment2 from '../../../view/img/triangle-white-right.svg';

import { dpath } from '../../Main'

class ServicesFirst extends Component{
    render(){

        return (
            <div className={"container"}>
                <img className="fragment-bg first" src={bgFragment} alt=""/>
                <img className="fragment-bg second" src={bgFragment2} alt=""/>
                <img className="background-symbol first" src={require('../../../view/img/background_symbol2.svg')} />
                <img className="background-symbol second" src={require('../../../view/img/background_symbol3.svg')} />
                <div className="text d-flex f-column f-between">
                    <p dangerouslySetInnerHTML={{__html: this.props.content_1}}></p>
                    <p dangerouslySetInnerHTML={{__html: this.props.content_2}}></p>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(ServicesFirst));
