import React, {Component} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';




class FourScreen extends Component{

    render(){
      let lang = '';
      let mailto = '';
      let tel = '';
      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
        tel = 'tel:'+lang.kontaktinis_telefonas;
        mailto = 'mailto:'+lang.kontaktinis_elpastas;

      }

        return (
          /*
          <div className={"footer-element"} data-visible={this.props.visible}>
              <div className="container">
              <img src={require('../../view/img/9b-background.svg')} alt="" className="bottom-bg"/>
              <div className="bottom">
                  <div className="contact-us d-flex f-between">
                      <div className="left">
                          <h4>Contact us</h4>
                          <p>Get specific information
                              for your concrete business:</p>
                      </div>
                      <div className="right d-flex f-align-center">
                          <div className="block d-flex f-align-center">
                              <img src={require('../../view/img/9-contacts_phone.svg')} alt=""/>
                              <Link to="tel:+37052055244">
                                  <span>+370 520 55 244</span>
                              </Link>
                          </div>
                          <div className="block d-flex f-align-center">
                              <img src={require('../../view/img/9-contacts_mail.svg')} alt=""/>
                              <Link to="mailto:info@fininbox.com">
                                  <span>info@fininbox.com</span>
                              </Link>
                          </div>
                      </div>
                  </div>
                  <footer>
                      <div className="d-flex f-between">
                          <Link to="/" className="logo">
                              <img src={require('../../view/img/logo-footer.svg')} alt=""/>
                          </Link>
                          <ul className="center d-flex f-center f-align-center">
                              <li><Link to="/legal">Legal</Link></li>
                              <li><Link to="/conditions">Terms & Conditions</Link></li>
                          </ul>
                          <div className="right">
                              <span>find us:</span>
                              <img src={require('../../view/img/9-linkedin.svg')} alt=""/>
                          </div>
                      </div>
                  </footer>
              </div>
          </div>
          </div>
          */



          <div className="container">
              <img src={require('../../view/img/9b-background.svg')} alt="" className="bottom-bg"/>
              <div className="bottom">
                  <div className="contact-us d-flex f-between">
                      <div className="left">
                          <h4>{lang.contact_us}</h4>
                          <p>{lang.contactus_text}</p>
                      </div>
                      <div className="right d-flex f-align-center">
                          <div className="block d-flex f-align-center">
                              <img src={require('../../view/img/9-contacts_phone.svg')} alt=""/>
                              <a href={tel}>
                                  <span>{lang.kontaktinis_telefonas}</span>
                              </a>
                          </div>
                          <div className="block d-flex f-align-center">
                              <img src={require('../../view/img/9-contacts_mail.svg')} alt=""/>
                              <a href={mailto}>
                                  <span>{lang.kontaktinis_elpastas}</span>
                              </a>
                          </div>
                      </div>
                  </div>
                  <footer>
                      <div className="d-flex f-between">
                          <Link to="/" className="logo">
                              <img src={require('../../view/img/logo-footer.svg')} alt=""/>
                          </Link>
                          <ul className="center d-flex f-center f-align-center">
                              <li><a href={lang.legal_link}>{lang.legal}</a></li>
                              <li><a href={lang.terms_conditions_link}>{lang.terms_conditions}</a></li>
                          </ul>
                          <div className="right">
                              <span>{lang.find_us}:</span>
                              <a href={lang.linkedin} target="_blank">
                              <img src={require('../../view/img/9-linkedin.svg')} alt=""/>
                              </a>
                          </div>
                      </div>
                  </footer>
              </div>
          </div>



        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(FourScreen));
