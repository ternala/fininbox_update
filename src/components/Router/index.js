import React, {Component} from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Redirect from "react-router/es/Redirect";



import Home from '../Home/';
import Functionality from "../Functionality";
import ContactUs from "../ContactUs";
import Services from "../Services";
import About from "../About";
import Developers from "../Developers";
import Legal from "../Legal";
import TermsConditions from "../TermsConditions";


class Router extends Component{
    render(){
        /*
      let ptypes = ['sk','home','functionality','contacts','services','about','developers',''];
      let path = this.props.ownProps.location.pathname;
      path = path.replace('/','');
      let dex = ptypes.indexOf(path);

      let pages = [];

    if (!dex) {
      pages.push(
     <Route path="/{path}" render={()=>(
            <Legal showHeader={this.props.showHeader} lang={this.props.lang} hideHeader={this.props.hideHeader}/>)
        } />

      )

    }  */


        return (
            <Switch>
                <Route path="/functionality" render={()=>(
                    <Functionality showHeader={this.props.showHeader} lang={this.props.lang} hideHeader={this.props.hideHeader}/>)
                } />
                <Route path="/contacts" render={()=>(
                    <ContactUs showHeader={this.props.showHeader} lang={this.props.lang} hideHeader={this.props.hideHeader}/>)
                } />
                <Route path="/services" render={()=>(
                    <Services showHeader={this.props.showHeader} lang={this.props.lang} hideHeader={this.props.hideHeader}/>)
                } />
                <Route path="/about" render={()=>(
                    <About showHeader={this.props.showHeader} lang={this.props.lang} hideHeader={this.props.hideHeader}/>)
                } />
                <Route path="/conditions" render={()=>(
                    <TermsConditions lang={this.props.lang}/>)
                } />
                <Route path="/legal" render={()=>(
                    <Legal lang={this.props.lang}/>)
                } />
                <Route path="/developers" render={()=>(
                    <Developers lang={this.props.lang}/>)
                } />
                <Route path="/" render={()=>(
                    <Home showHeader={this.props.showHeader} hideHeader={this.props.hideHeader} lang={this.props.lang}/>)
                } />
                <Route component={() => <Redirect to="/" />}/>
            </Switch>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(Router));
