import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Swiper from 'react-id-swiper';

import triangleBg from '../../../view/img/2-background-grey.svg';
import iconBanking from '../../../view/img/services_banking.svg';
import iconLegal from '../../../view/img/services_legal.svg';
import iconPayments from '../../../view/img/services_payments.svg';


class FunctionalitySecond extends Component{
    constructor(props){
        super(props);
        this.state ={
            choosedTab: false,
            hoveredElement: false,
        };
        this.chooseTab = this.chooseTab.bind(this);
        this.closeTab = this.closeTab.bind(this);
        this.onMouseEnterHandler = this.onMouseEnterHandler.bind(this);
        this.onMouseLeaveHandler = this.onMouseLeaveHandler.bind(this);
    }

    chooseTab(e){
        this.setState({
            choosedTab:parseInt(e.currentTarget.getAttribute('data-id-tab'))
        });
        let _this = this;
        setTimeout(function(){
            if (_this.swiper){
                _this.swiper.update();
            }
            if (_this.swiper2){
                _this.swiper2.update();
            }
        }, 500);
        setTimeout(function(){
            if (_this.swiper){
                _this.swiper.update();
            }
            if (_this.swiper2){
                _this.swiper2.update();
            }
        }, 1500)
    }

    closeTab(e){
        this.setState({
            choosedTab:false
        });
    }

    onMouseEnterHandler(e) {
        this.setState({
            hoveredElement: parseInt(e.currentTarget.getAttribute('data-id-tab'))
        });
    }

    onMouseLeaveHandler(e) {
        this.setState({
            hoveredElement: false
        });
    }

    render(){
        const params = {
            direction: 'vertical',
            slidesPerView: 1,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                600:{
                    direction: 'horizontal',
                }
            }
        };

        let con = this.props.content;

        let pict = '';
        let paslaugos_b1 = [];
        let paslaugos_b2 = [];
        let paslaugos_b3 = [];
        let blocksArr1 = [];
        let blocksArr2 = [];
        let blo1 = [];
        if (typeof this.props.paslaugosb_1 != 'undefined') {
          paslaugos_b1 = this.props.paslaugosb_1;

        if (paslaugos_b1[0]){
          blo1.push(
          <div className={"block"
          + (this.state.choosedTab === 1?' active':'')
          + (this.state.hoveredElement === 1 ? ' hovered':'')
          + (this.state.hoveredElement !== false && this.state.hoveredElement !== 1 ? ' another-hovered':'')}
               data-id-tab="1" onClick={this.chooseTab}
               onMouseEnter={this.onMouseEnterHandler}
               onMouseLeave={this.onMouseLeaveHandler}>
              <div className="wrapper">
                  <div className="inner">
                      <img src={iconBanking} alt=""/>
                      <h3>{paslaugos_b1[0]}</h3>
                      <h4>{paslaugos_b1[1]}</h4>
                  </div>
              </div>
          </div>
        );
      }

      if (typeof this.props.paslaugost_1 != 'undefined')  {
        Object.keys( this.props.paslaugost_1 ).map(key=>{
          blocksArr1.push(
                <div dangerouslySetInnerHTML={{__html: this.props.paslaugost_1[key].text}}></div>
          )
        })
      }

    }
    if (typeof this.props.paslaugosb_2 != 'undefined') {
      paslaugos_b2 = this.props.paslaugosb_2;
      if (paslaugos_b2[0]){
        blo1.push(
          <div className={"block"
          + (this.state.choosedTab === 2?' active':'')
          + (this.state.hoveredElement === 2 ? ' hovered':'')
          + (this.state.hoveredElement !== false && this.state.hoveredElement !== 2 ? ' another-hovered':'')}
               data-id-tab="2" onClick={this.chooseTab}
               onMouseEnter={this.onMouseEnterHandler}
               onMouseLeave={this.onMouseLeaveHandler}>
              <div className="wrapper">
                  <div className="inner">
                      <img src={iconPayments} alt=""/>
                      <h3>{paslaugos_b2[0]}</h3>
                      <h4>{paslaugos_b2[1]}</h4>
                  </div>
              </div>
          </div>
      );
    }
    if (typeof this.props.paslaugost_2 != 'undefined')  {
      Object.keys( this.props.paslaugost_2 ).map(key=>{
        blocksArr2.push(
              <div dangerouslySetInnerHTML={{__html: this.props.paslaugost_2[key].text}}></div>
        )
      })
    }

  }
  if (typeof this.props.paslaugosb_3 != 'undefined') {

    paslaugos_b3 = this.props.paslaugosb_3;
    if (paslaugos_b3[0]){
      blo1.push(
        <div className={"block"
        + (this.state.choosedTab === 3 ? '  active':'')
        + (this.state.hoveredElement === 3 ? ' hovered':'')
        + (this.state.hoveredElement !== false && this.state.hoveredElement !== 3 ? ' another-hovered':'')}
             data-id-tab="3"
             onClick={this.chooseTab}
             onMouseEnter={this.onMouseEnterHandler}
             onMouseLeave={this.onMouseLeaveHandler}
            >
            <div className="wrapper">
                <div className="inner">
                    <img src={iconLegal} alt=""/>
                    <h3>{paslaugos_b3[0]}</h3>
                    <h4>{paslaugos_b3[1]}</h4>
                </div>
            </div>
        </div>
    );
   }

        }





        return (
            <div>
                <img src={triangleBg} className="fragment-bg" alt=""/>
                <div className={"blocks" + (this.state.choosedTab?' choosed':'')}>
                    <div className="close" onClick={this.closeTab}>
                        <i></i><i></i>
                    </div>

                    <div className="wrapper-for-blocks">


                        {blo1}



                    </div>
                    <div className="contents">
                        <div className={"content" + (this.state.choosedTab === 1 ? ' show':'')} data-id-tab="1">
                            <Swiper {...params} ref={node => {if(node) this.swiper = node.swiper }}>
                                {blocksArr1}
                            </Swiper>
                        </div>
                        <div className={"content" + (this.state.choosedTab === 2 ? ' show':'')} data-id-tab="2">
                          <Swiper {...params} ref={node => {if(node) this.swiper2 = node.swiper }}>
                            {blocksArr2}
                          </Swiper>
                        </div>
                        <div className={"content" + (this.state.choosedTab === 3 ? ' show':'')} data-id-tab="3" dangerouslySetInnerHTML={{__html: this.props.paslaugost_3}}>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(FunctionalitySecond));
