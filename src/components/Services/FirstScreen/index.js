import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import bgFragment from '../../../view/img/1-background-60-degrees-cut.svg';

class ServicesFirst extends Component{
    constructor(props){
        super(props);
        this.state = {
            loaded:false
        };
    }
    componentDidMount(){

    }
    componentDidUpdate(){

        if(!this.state.loaded){
            let _this = this;
            setTimeout(function(){
                _this.setState({
                    loaded:true
                })
            }, 300);
        }
    }

    render(){
        return (
            <div className={"container" + (this.state.loaded?' loaded':'')}>
                <img className="fragment-bg" src={bgFragment} alt=""/>
                <img className="background-symbol second" src={require('../../../view/img/background_symbol3.svg')} />
                <img className="background-symbol first" src={require('../../../view/img/background_symbol3.svg')} />
                <div className="content">
                    <div className="innner-content">
                        <div className="by-top">
                        <h2>{this.props.title}</h2>
                        <p dangerouslySetInnerHTML={{__html: this.props.content}}></p>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(ServicesFirst));
