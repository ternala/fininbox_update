import React, {Component, Fragment} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

class EightScreen extends Component{
    render(){
        return (
            <div className="container">
                <img src={require('../../../view/img/9-background.svg')} alt="" className="top-bg"/>
                <img src={require('../../../view/img/9b-background.svg')} alt="" className="bottom-bg"/>
                <div className="top d-flex f-between">
                    <div className="left-content">
                        <h3 dangerouslySetInnerHTML={{__html: this.props.title1}}></h3>
                        <p dangerouslySetInnerHTML={{__html: this.props.content1}}></p>
                    </div>
                    <div className="btn-play">
                        <a href={this.props.link} className="small-btn">
                            <div className="text">
                                <h3 dangerouslySetInnerHTML={{__html: this.props.title2}}></h3>
                                <div className="arrow">
                                    <hr/><i></i>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="bottom">
                    <div className="contact-us d-flex f-between">
                        <div className="left">
                            <h4>Contact us</h4>
                            <p>Get specific information
                                for your concrete business:</p>
                        </div>
                        <div className="right d-flex f-align-center">
                            <div className="block d-flex f-align-center">
                                <img src={require('../../../view/img/9-contacts_phone.svg')} alt=""/>
                                <Link to="tel:+37052055244">
                                    <span>+370 520 55 244</span>
                                </Link>
                            </div>
                            <div className="block d-flex f-align-center">
                                <img src={require('../../../view/img/9-contacts_mail.svg')} alt=""/>
                                <Link to="mailto:info@fininbox.com">
                                    <span>info@fininbox.com</span>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <footer>
                        <div className="d-flex f-between">
                            <Link to="/" className="logo">
                                <img src={require('../../../view/img/logo-footer.svg')} alt=""/>
                            </Link>
                            <ul className="center d-flex f-center f-align-center">
                                <li><Link to="">Legal</Link></li>
                                <li><Link to="">Terms & Conditions</Link></li>
                            </ul>
                            <div className="right">
                                <span>find us:</span>
                                <img src={require('../../../view/img/9-linkedin.svg')} alt=""/>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(EightScreen));
