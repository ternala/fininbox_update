import React, {Component, Fragment} from 'react';
import ReactPlayer from 'react-player';

export default class Video extends Component {
   render () {
       let { fullpageApi} = this.props;
        return (
            <div>
                <ReactPlayer url='/first-screen-video.mp4' muted={true} playing={this.props.videoPlay} onReady={this.props.setVideoReady}/>
                <div className="next-section" onClick={fullpageApi?fullpageApi.moveSectionDown:()=>false}>
                    <span>explore</span>  it
                    <img src={require('../../../../view/img/arrow-to-bottom.svg')}/>
                </div>
            </div>

        )
    }
}