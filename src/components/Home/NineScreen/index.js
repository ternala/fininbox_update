import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

class EightScreen extends Component{
    render(){
      let con = this.props.blocks;
      let blocksArr = [];
      let pos = ["top", "right", "bottom", "left"];

      if (typeof con != 'undefined') {
        Object.keys( con ).map(key=>{

          blocksArr.push(


            <div className={ pos[key] }>
                <h3>{ con[key].text1 }</h3>
                <p>{ con[key].text2 }</p>
            </div>


          )
        })
      }

        return (
            <div className="container">
                <div className="content">
                    {  blocksArr }
                </div>
                <div className="bgs">
                    <div className="bg bg-bottom"></div>
                    <div className="bg bg-top"></div>
                    <div className="bg bg-purple"></div>
                    <div className="bg bg-yellow"></div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(EightScreen));
