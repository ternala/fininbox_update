import React, {Component, Fragment} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { dpath } from '../../Main'

class SixScreen extends Component{
    render(){
      let lang = '';

      if (typeof this.props.lang != 'undefined') {
        lang = this.props.lang;
      }

      let con = this.props.blocks;
      let blocksArr = [];
      if (typeof con != 'undefined') {
        Object.keys( con ).map(key=>{

          blocksArr.push(

            <div className="block-3">
                <div className="inner">
                    <img src={ dpath + con[key].picture } alt=""/>
                    <h5>{ con[key].name }</h5>
                </div>
            </div>

          )
        })
      }


        return (
            <div className="container">

                <h2 dangerouslySetInnerHTML={{__html: this.props.title}}>
                </h2>
                <p dangerouslySetInnerHTML={{__html: this.props.content}}>
                </p>
                <div className="we-fully-cover">
                    <h4>{lang.pilnai_padengiame}</h4>
                    <div className="d-flex">

{blocksArr}

                        
                    </div>
                </div>
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(SixScreen));
