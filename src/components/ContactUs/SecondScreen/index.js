import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

Math.degrees = function(radians) {
    return radians * 180 / Math.PI;
};

class SimpleMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0,
            height: 0,
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    renderMarkers(map, maps) {
        let marker = new maps.Marker({
            position: {lat: 54.702959, lng: 25.290392},
            map,
            title: 'Fininbox',
            icon: require('../../../view/img/marker-map.png')
        });
    }

    render() {
        let flag = this.props.state.destination?this.props.state.destination.index === 1||this.props.state.destination.index === 2:false;
        let angle = 45;
        let zoom = 17;
        if( this.state.width <= 992 ){
            zoom = 16;
        }

        if(flag){
            if(this.state.width > 992){
                angle = Math.degrees(Math.atan((this.state.height*1.1/2)/(this.state.width*0.7)));
            }else{
                angle = Math.degrees(Math.atan((this.state.height*1.1/2)/(this.state.width*0.95)));
            }

        }else{
            if(this.state.width > 992) {
                angle = Math.degrees(Math.atan((this.state.height * 0.6 / 2) / (this.state.width * 0.45)));
            }else{
                angle = Math.degrees(Math.atan((this.state.height * 0.6 / 2) / (this.state.width * 0.8)));
            }
        }

        let { fullpageApi } = this.props;

        let lang = '';
        let apikey = '';
        let mapcoord = '';
        if (typeof this.props.lang != 'undefined') {
          lang = this.props.lang;
          apikey = 'key: '+lang.zemelapis_api;
          mapcoord = lang.kontaktinis_zemelapis;

        }


console.log(mapcoord);
        return (
            <div className="wrapper-map">
                <div className="top path" style={{transform: `rotate(-${angle}deg)`}}></div>
                <div className="bottom path" style={{transform: `rotate(${angle}deg)`}}></div>
                <div className="inner-map" >
                    <div className="prevent-click" onClick={fullpageApi?fullpageApi.moveSectionDown:()=>false}>
                    </div>
                    <div style={{ height: '100%', width: '100%' }}>
                        <GoogleMapReact
                            bootstrapURLKeys={{key: "AIzaSyDsiAqp-ZEVgUwVjsbvrGFzNvkNGTf_IQY"}}
                            defaultCenter={{lat: 54.702959, lng: 25.290392}}
                            defaultZoom={zoom}
                            options={{gestureHandling: "none"}}
                            onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
                        >
                        </GoogleMapReact>
                    </div>
                </div>
            </div>
        );
    }
}

export default SimpleMap;
