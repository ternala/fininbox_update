import React, {Component} from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

class SecondScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            loaded:false
        }
    }
    componentDidMount(){
        this.setState({
            loaded:true
        })
    }
    render(){
        return (
            <div className={"container" + (this.state.loaded?' loaded':'')}>
                <img src={require('../../../view/img/background-contact-us.svg')} alt="" className="purple-fragment fragment-bg"/>
                <div className="content">
                    <h1>{this.props.title}</h1>
                    <div className="d-flex">
                        <div className="block-4">
                            <h2>{this.props.content0}</h2>
                        </div>
                        <div className="block-4 center">
                            <div className="dib" dangerouslySetInnerHTML={{__html: this.props.content}}>
                            </div>
                        </div>
                        <div className="block-4 right">
                            <div className="dib" dangerouslySetInnerHTML={{__html: this.props.content2}}>
                            </div>
                        </div>
                    </div>
                </div>
                <img className="background-symbol first" src={require('../../../view/img/background_symbol2.svg')} />
            </div>
        );
    }
};

export default withRouter(connect(
    (state, ownProps) => ({
        ownProps
    })
)(SecondScreen));
